using UnityEngine;

[CreateAssetMenu(menuName = "StackCubesConfig", fileName = "stackCubesConfig")]
public class StackConfig : ScriptableObject
{
 public float startSizeX = 1;
 public float startSizeZ = 1;
 public float startSizeY = 0.2f;
 public float colorUpdateStep = 0.05f;
 public float moveCubeSpeed;
 public Vector2 boundsX = new Vector2(-10f, 10f);
// public Vector2 boundsZ = new Vector2(-10f, 10f);
 private Vector3 startCubeScale;

 private void OnEnable()
 {
  startCubeScale = Vector3.zero;
 }

 public Vector3 StartCubeScale
 {
  get
  {
   if (startCubeScale == Vector3.zero)
   {
    startCubeScale = new Vector3(startSizeX, startSizeY, startSizeZ);
    return startCubeScale;
   }
   else
   {
    return startCubeScale;
   }
  }
 }
 
}
