using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class NewRenderer : MonoBehaviour
{
  private Mesh mesh;

  public void GenerateMesh(float x, float y, float z)
  {
    mesh = new Mesh();
    GetComponent<MeshFilter>().mesh = mesh;
    mesh.vertices = GenerateVertices(x,y,z);
    mesh.triangles = GenerateTriangles();
  }

  Vector3[] GenerateVertices(float x, float y, float z)
  {
    
    return new Vector3[]
    {
    new Vector3(-x/2, -y/2, -z/2), //0
    new Vector3(-x/2, -y/2, z/2), //1
    new Vector3(x/2, -y/2, -z/2), //2
    new Vector3(x/2, -y/2, z/2), 
    new Vector3(-x/2, y/2, -z/2),
    new Vector3(x/2, y/2, -z/2),
    new Vector3(-x/2, y/2, z/2),
    new Vector3(x/2, y/2, z/2),
    };
  }

  int[] GenerateTriangles()
  {
    return new int[]
    {
    0,4,2, 2,4,5,
    0,1,4, 1,6,4,
    4,6,7, 4,7,5,
    2,5,3, 3,5,7,
    4,1,7, 6,1,4,
    6,1,3, 6,3,7,
    0,1,4, 4,2,0,
    0,2,3, 3,1,0
    };
  }
  
}
