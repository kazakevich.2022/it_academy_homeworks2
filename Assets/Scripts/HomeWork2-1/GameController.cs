using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    [SerializeField] private StackConfig gameConfig;
    [SerializeField] private Camera gameCamera;
    
    
    private Transform lastSpawnedCube;
    private Transform lastStackedCube;
    private Vector3 lastStacketCubeLocalScale;
    
    private Color startColor;
    private Color targetColor;
    private Color currentColor;
    private float colorLerpCoefficient = 0;
    private int cubesCount;
    public float currentSpawnCoordinateY;
    private Coroutine moveObjectCoroutine;
    public NewRenderer rendererCube;

    private float currentSizeX;
    private float currentSizeZ;
    private void Start()
    {
        lastStacketCubeLocalScale = gameConfig.StartCubeScale;
        currentSizeX = gameConfig.startSizeX;
        currentSizeZ = gameConfig.startSizeZ;
        startColor = Random.ColorHSV();
        currentColor = startColor;
        //AddColor();
        SetupStartCubes(3);
        SpawnMovedCube();
    }
    
    private void SetupStartCubes(int startCubesCount)
    {
        for (int i = 0; i < startCubesCount; i++)
        {
            SpawnStackedCube(new Vector3(0,i*gameConfig.startSizeY,0), gameConfig.StartCubeScale);
        }
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StopAndSetupNewCube();
        }
    }
    private void StopAndSetupNewCube()
    {
        if (moveObjectCoroutine != null)
        {
            StopCoroutine(moveObjectCoroutine);
            moveObjectCoroutine = null;
        }
        lastStackedCube = StackCube(lastStackedCube,lastSpawnedCube);
        Destroy(lastSpawnedCube.gameObject);
        if (lastStackedCube == null)
        {
            print($"you lose : cubes count {cubesCount-1}");
            return;
        }
        
        SpawnMovedCube();
    }
    
    private Transform StackCube(Transform lastStackedCube, Transform lastSpawnedCube)
    {
        if (Mathf.Abs(lastSpawnedCube.position.x - lastStackedCube.position.x) >= currentSizeX)
        {
            return null;
        }
        
        
        NewRenderer newBigCubeRenderer = Instantiate(rendererCube);
        Transform stackedCube = newBigCubeRenderer.transform;
        Debug.Log(currentColor);
        newBigCubeRenderer.GetComponent<MeshRenderer>().material.color = currentColor;
        //big cube
        stackedCube.position = new Vector3((lastSpawnedCube.position.x - lastStackedCube.position.x)/2+ lastStackedCube.position.x,
            lastSpawnedCube.position.y, lastSpawnedCube.position.z);
        float differenceInScale = Mathf.Abs(lastSpawnedCube.position.x - lastStackedCube.position.x);
        currentSizeX -= differenceInScale;

        lastStacketCubeLocalScale = new Vector3(currentSizeX, gameConfig.startSizeY, currentSizeZ);
        stackedCube.gameObject.name = (cubesCount-1).ToString();
        
        newBigCubeRenderer.GenerateMesh(currentSizeX,gameConfig.startSizeY,currentSizeZ);

        NewRenderer newSmallCubeRenderer = Instantiate(rendererCube);
        
        var smallCube =newSmallCubeRenderer.transform;
        if (stackedCube.position.x > lastSpawnedCube.position.x)
        {
            smallCube.position = new Vector3(stackedCube.position.x-lastSpawnedCube.localScale.x/2,
                lastSpawnedCube.position.y, lastSpawnedCube.position.z);
        }
        else
        {
            smallCube.position = new Vector3(stackedCube.position.x+lastSpawnedCube.localScale.x/2,
                lastSpawnedCube.position.y, lastSpawnedCube.position.z);
        }
        
        smallCube.gameObject.AddComponent<Rigidbody>();
        newSmallCubeRenderer.GetComponent<MeshRenderer>().material.color = currentColor;
        newSmallCubeRenderer.GenerateMesh(differenceInScale,gameConfig.startSizeY,currentSizeZ);
        Destroy(smallCube.gameObject, 1f);
        return stackedCube;
    }
    private void AddColor()
    { 
        Debug.Log(currentColor);
        colorLerpCoefficient += gameConfig.colorUpdateStep;
        if (colorLerpCoefficient > 1f)
        {
            startColor = targetColor;
            targetColor = Random.ColorHSV();
            colorLerpCoefficient -= 1;
        }
        currentColor = Color.Lerp(startColor,targetColor, colorLerpCoefficient);
    }
    private void SpawnMovedCube()
    {
        Vector3 spawnCubeScale = lastStacketCubeLocalScale;
        bool isMinPos = Random.Range(0, 2) == 0;
        Vector3 spawnPos;
        if (isMinPos)
        {
            //min pos x
            spawnPos = new Vector3(gameConfig.boundsX.x, currentSpawnCoordinateY+ gameConfig.startSizeY, lastSpawnedCube.position.z);
        }
        else
        {
            //max pos x
            spawnPos = new Vector3(gameConfig.boundsX.y, currentSpawnCoordinateY + gameConfig.startSizeY, lastSpawnedCube.position.z);
        }

        var coefficientPingPong = 0f;

       
        lastSpawnedCube = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
        lastSpawnedCube.gameObject.name = cubesCount.ToString();
        lastSpawnedCube.GetComponent<MeshRenderer>().material.color = currentColor;
        lastSpawnedCube.position = spawnPos;
        lastSpawnedCube.localScale = spawnCubeScale;

        cubesCount++;
        
        currentSpawnCoordinateY = lastSpawnedCube.position.y;
        gameCamera.transform.position += Vector3.up * gameConfig.startSizeY;
        AddColor();
        if (isMinPos)
        {
            moveObjectCoroutine = StartCoroutine(MoveObjectCoroutineCycled(lastSpawnedCube,
                new Vector3(gameConfig.boundsX.x, currentSpawnCoordinateY, lastSpawnedCube.position.z),
                new Vector3(gameConfig.boundsX.y, currentSpawnCoordinateY, lastSpawnedCube.position.z), gameConfig.moveCubeSpeed));
        }
        else
        {
            moveObjectCoroutine = StartCoroutine(MoveObjectCoroutineCycled(lastSpawnedCube,
                new Vector3(gameConfig.boundsX.y, currentSpawnCoordinateY, lastSpawnedCube.position.z),
                new Vector3(gameConfig.boundsX.x, currentSpawnCoordinateY, lastSpawnedCube.position.z),
                gameConfig.moveCubeSpeed));
        }
    }

    private GameObject SpawnStackedCube(Vector3 spawnPos, Vector3 startScale)
    {
        lastStackedCube = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
        lastStackedCube.gameObject.name = cubesCount.ToString();
        lastStackedCube.GetComponent<MeshRenderer>().material.color = currentColor;
        lastStackedCube.position = spawnPos;
        lastStackedCube.localScale = startScale;
        lastSpawnedCube = lastStackedCube;
        cubesCount++;
        
        currentSpawnCoordinateY = lastStackedCube.position.y;
        gameCamera.transform.position += Vector3.up * gameConfig.startSizeY;
        AddColor();
        return lastStackedCube.gameObject;
    }
    private IEnumerator MoveObjectCoroutineCycled(Transform moveObject, Vector3 startPos, Vector3 endPos, float moveSpeed = 1)
    {
        var lerpCoefficient = 0f;
        while (true)
        {
            lerpCoefficient += Time.deltaTime * moveSpeed;
            moveObject.position = Vector3.Lerp(startPos, endPos,Mathf.PingPong(lerpCoefficient,1));
            yield return null;
        }
    }
    
}


