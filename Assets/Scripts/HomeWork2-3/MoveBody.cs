using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBody : MonoBehaviour
{
    public float movementSpeed;
    public float rotationSpeed;
    private Rigidbody body;
    
    public GameObject bodyChildfGun;
    public GameObject bodyChildGranadeGun;
    public GameObject bodyChildTennisBool;
    
    public TriggerZone leftZone;
    public TriggerZone rightZone;
    public TriggerZone centerZone;
    public static TypeOfGun TypeOfGun = TypeOfGun.None;
  
    void Start()
    {
        rightZone.OnEnterTriggerZone += () =>
        {
            bodyChildfGun.gameObject.SetActive(true);
            TypeOfGun = TypeOfGun.Bullet;
        };
        rightZone.OnExitTriggerZone += () =>
        {
            bodyChildfGun.gameObject.SetActive(false);
            TypeOfGun = TypeOfGun.None;
        };
        centerZone.OnEnterTriggerZone += () =>
        {
            bodyChildGranadeGun.gameObject.SetActive(true);
            TypeOfGun = TypeOfGun.Granade;
        };
        centerZone.OnExitTriggerZone += () =>
        {
            bodyChildGranadeGun.gameObject.SetActive(false);
            TypeOfGun = TypeOfGun.None;
        };
        leftZone.OnEnterTriggerZone += () =>
        {
            bodyChildTennisBool.gameObject.SetActive(true);
            TypeOfGun = TypeOfGun.TennisBall;
        };
        leftZone.OnExitTriggerZone += () =>
        {
            bodyChildTennisBool.gameObject.SetActive(false);
            TypeOfGun = TypeOfGun.None;
        };
        
        
        
            body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float sideForce = Input.GetAxis("Horizontal") * rotationSpeed;
        if (sideForce != 0.0f)
        {
            body.angularVelocity = new Vector3(0.0f, sideForce, 0.0f);
        }

        float forwardForce = Input.GetAxis("Vertical") * movementSpeed;
        if (forwardForce != 0.0f)
        {
            body.velocity = body.transform.forward * forwardForce;
        }

        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnTriggerEnter - " + other);
        
    }

    private void OnTriggerStay(Collider other)
    {
       
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("OnTriggerExit - " + other);
    }

//     private void OnCollisionEnter(Collision collision)
//     {
//         Debug.LogWarning("OnCollisionEnter - " + collision.collider+ "-" +collision.impulse);
//     }
 }
[Serializable]
public enum TypeOfGun
{
    Granade= 1,
    Bullet =2,
    TennisBall =3,
    None =4,
}
