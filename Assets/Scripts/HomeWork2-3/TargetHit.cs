using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetHit : MonoBehaviour
{
    [SerializeField] private ParticleSystem fireBallBullet;

    public void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.CompareTag("Bullits"))
        {
            var effectFireBall = Instantiate(fireBallBullet);
         effectFireBall.transform.position = transform.position;
         effectFireBall.Play();
         Destroy(effectFireBall.gameObject,2.0f);
        
         other.gameObject.SetActive(false);
         gameObject.SetActive(false);
           
        }
    }
}
