using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UIElements;

public class Bullet : MonoBehaviour
{
  
    public float speed;
    private Vector3 lastPos;
    private Rigidbody rgBullet;
    public Vector3 startBulletPos;
    [SerializeField] private ParticleSystem FlameStream;

    
    //POOl
   
    [SerializeField] private int poolCount = 3;
    [SerializeField] private bool autoExpand = false;
    [SerializeField] private FlyBullet bulletPrefabs;
    private PoolObjects<FlyBullet> pool;
    //
  

    void Start()
    {
        // pool
        this.pool = new PoolObjects<FlyBullet>(this.bulletPrefabs, this.poolCount, transform)
        {
            autoExpand = this.autoExpand
        };

        rgBullet = GetComponent<Rigidbody>();
       
        
     
    }

 void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ShootingBullet();
            switch (MoveBody.TypeOfGun)
            {
                case TypeOfGun.Granade:
                    AudioManager.Instance.PlayGrenadShoot();
                 break;
                case TypeOfGun.Bullet:
                    AudioManager.Instance.PlayShoot();
                    break;
                case TypeOfGun.TennisBall:
                    AudioManager.Instance.PlayTennisBoll();
                    break;
                case TypeOfGun.None:
                    break;
            }
        }
       
        
    }


 private void ShootingBullet()
 {
     //
     var freeElement = this.pool.GetFreeElement();
     freeElement.transform.localPosition = startBulletPos;
     freeElement.transform.localRotation = Quaternion.Euler(Vector3.zero);
   
     rgBullet = freeElement.GetComponent<Rigidbody>();
     rgBullet.velocity = Vector3.zero;
     rgBullet.AddForce( (freeElement.transform.position - transform.position).normalized*speed, ForceMode.Force);

   }

 
  
}



