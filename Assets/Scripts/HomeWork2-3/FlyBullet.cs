using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyBullet : MonoBehaviour
{
    [SerializeField] private float lifeTime = 3f;
    [SerializeField] private ParticleSystem explosionEffect;
  //  [SerializeField] private ParticleSystem fireBallBullet;
   
    private void OnEnable()
    {
        this.StartCoroutine("LifeObjects");
    }

    private void OnDisable()
    {
        this.StopCoroutine("LifeObjects");
    }

    private IEnumerator LifeObjects()
    {
        yield return new WaitForSecondsRealtime(this.lifeTime);
        this.Deactivate();
    }

    private void Deactivate()
    {
        this.gameObject.SetActive(false);
        explosionEffect.Stop();
        explosionEffect.time = 0;
    }
    
    public void OnCollisionEnter(Collision other)
    {
        
        // if (other.gameObject.CompareTag("TargetGun"))
        // {
        //   
        //     Destroy(other.gameObject);
        //     //Destroy(gameObject);
        //    gameObject.SetActive(false);
        // }
        
        
        if (other.gameObject.CompareTag("GranadeTagTarget"))
        {
            AudioManager.Instance.PlayExplosion();
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(GameSettings.power,gameObject.transform.position,GameSettings.GRANADE_EXPLOSION_RADIUS,1.0f);
            }
            
            StartCoroutine(EffectCoroutine());
        }

    }

    private IEnumerator EffectCoroutine()
    {
        var effect = Instantiate(explosionEffect);
        effect.transform.position = transform.position;
        effect.Play();
        Destroy(effect.gameObject,2.0f);
        yield return null;
        
    }
    
}
