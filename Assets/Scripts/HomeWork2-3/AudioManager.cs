using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField] public AudioSource effectAudioSource;
    [SerializeField] public AudioSource ambientAudioSource;

    [SerializeField] private AudioClip shoot;
    [SerializeField] private AudioClip explosion;
    [SerializeField] private AudioClip slap;
    [SerializeField] private AudioClip dsgetpow;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void PlayShoot()
    {
         effectAudioSource.PlayOneShot(shoot);
    }
    public void PlayGrenadShoot()
    {
        effectAudioSource.PlayOneShot(dsgetpow);
    }
    public void PlayExplosion()
    {
        effectAudioSource.PlayOneShot(explosion);
    }
    public void PlayTennisBoll()
    {
        effectAudioSource.PlayOneShot(slap);
    }

}
