using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorEllen : MonoBehaviour
{
    [SerializeField] private Animator anim;

    public GameObject grenader;
    public List<Rigidbody> elements;
    public Animator CharacterAnimator { get { return anim = anim ?? GetComponent<Animator>(); } }

    private void Start()
    {
        Destroy(grenader,20.48f);
        
    }

    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.K))
        {
            GetComponent<Animator>().enabled = false;
            //CharacterAnimator.Play("EllenDeath");
            foreach (var element in elements)
            {
                element.isKinematic = false;
            }
        }
    }
   
}
