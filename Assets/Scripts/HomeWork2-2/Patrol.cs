using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrol : MonoBehaviour
{

    public Transform[] points;

    int destPoint = 0;
    private NavMeshAgent agent;
   
    void Start()
    {
       agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        
        GoToNextPoint();
    }
    
    void Update()
    {
       if(!agent.pathPending && agent.remainingDistance <0.5f)
            GoToNextPoint();
    }
    void GoToNextPoint()
    {
        if (points.Length ==0) 
            return;
        agent.destination = points[destPoint].position;
        destPoint += 1 % points.Length;
        if (destPoint == 4)
        {
            destPoint = 0;
        }
    }
   
}
