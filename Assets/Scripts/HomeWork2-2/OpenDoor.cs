using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    public TriggerZone triggerZoneOpenDoor;
  

    
    private float speedOpenDoor = 0.2f;
    private Vector3 startPosDoor;
    private Vector3 endPosDoor;
    private float lerpCoeff;
    private float count;
    // Start is called before the first frame update
    void Awake()
    {
        startPosDoor = gameObject.transform.position;
        endPosDoor = new Vector3(transform.position.x, transform.position.y+3, transform.position.z);

        triggerZoneOpenDoor.OnEnterTriggerZone += () =>
        {
            CloseDoor();
        
        };
        triggerZoneOpenDoor.OnExitTriggerZone += () =>
        {
            OpenDoors();
        };
     
    }

    public void OpenDoors()
    {
     
        lerpCoeff += speedOpenDoor * Time.deltaTime;
        gameObject.transform.position = Vector3.Lerp(startPosDoor, endPosDoor, lerpCoeff);
    }
    
    public void CloseDoor()
    {
    
        lerpCoeff += speedOpenDoor * Time.deltaTime;
        gameObject.transform.position = Vector3.Lerp(endPosDoor, startPosDoor, lerpCoeff);
    }
    // Update is called once per frame
    void Update()
    {
     
    }
}
