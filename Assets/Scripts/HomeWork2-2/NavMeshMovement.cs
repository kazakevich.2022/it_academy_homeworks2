using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshMovement : MonoBehaviour
{

    private Camera mainCamera;
    private NavMeshAgent agent;

    public List<TriggerZone> listTriggerDeceleration;
    
    void Start()
    {
        foreach (var triggerDeceleration in listTriggerDeceleration)
        {
            triggerDeceleration.OnEnterTriggerZone += () =>
            {
                GetComponent<NavMeshAgent>().speed *= 0.5f;
            };
            triggerDeceleration.OnExitTriggerZone += () =>
            {
                GetComponent<NavMeshAgent>().speed *= 2.0f;
            };
        }
        
      
        mainCamera = Camera.main;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                agent.SetDestination(hit.point);
            }
            
        }
    }

}
